This repo contains the tools used for the publication
"Mining Performance Specifications"
Marc Brünink, David S. Rosenblum
FSE 2016
https://dl.acm.org/citation.cfm?id=2950314

Feel free to use it, but you are on your own.
In particular, I pushed the very last changes into final. This is not properly tested and not guaranteed to run.
develop is a previous snapshot that should work.
